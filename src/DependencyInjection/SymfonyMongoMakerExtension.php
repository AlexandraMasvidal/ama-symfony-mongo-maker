<?php

// Copyright (c) 2023 Alexandra Masvidal
// MIT License
// For the complete copyright and license information, please refer to the LICENSE file.

namespace Ama\SymfonyMongoMakerBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;


class SymfonyMongoMakerExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        //dd('DI Load');
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
    }


}