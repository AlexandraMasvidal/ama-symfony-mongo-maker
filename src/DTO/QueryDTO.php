<?php

namespace Ama\SymfonyMongoMakerBundle\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class QueryDTO
{

    public function __construct(

        #[Groups(["read","create","update"])]
        public readonly array|null $sort = null,

        #[Groups(["read","create","update"])]
        public readonly ?string $queryStrategy = null,

        public readonly array|null $nested = null,

        public readonly array|null $obj = null,

        public readonly array|null $arr = null,



    ) {}
}