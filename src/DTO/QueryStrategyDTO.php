<?php

namespace Ama\SymfonyMongoMakerBundle\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class QueryStrategyDTO
{

    public function __construct(

    // alternative
    #[Groups(["read","create","update"])]
    public readonly ?string $alt = null, 

    // aggregate
    #[Groups(["read","create","update"])]
    public readonly ?string $agg = null,

   
    
    ) {}
}