<?php

// Copyright (c) 2023 Alexandra Masvidal
// MIT License
// For the complete copyright and license information, please refer to the LICENSE file.

namespace Ama\SymfonyMongoMakerBundle\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'mongo:make:document',
    description: 'Creates a new document.'
)]
class MongoDocumentMakerCommand extends Command
{

    public function __construct()
    {
        parent::__construct();

    }

    public function configure() : void
    {
        $this
            ->setHelp('This command allows you to create a new document and his controller...')
            ->addArgument('name', InputArgument::OPTIONAL, 'The name of the document.')
            ->addOption('nocrud', null, InputOption::VALUE_NONE, 'Prevent odm annotations and controller creation');
    }

    public function execute($input, OutputInterface $output) : int
    {
        $io = new SymfonyStyle($input, $output);

        // get option crud (if the option is not set, the crud is true)
        $crud = !$input->getOption('nocrud');

        // get document name param if set
        $documentName = empty($input->getArgument('name')) ? '' : $input->getArgument('name');


        $updateExistingDocument = false;

        // handle the name of the new document
        if(empty($documentName)) {
            $documentName = $io->ask('What is the name of your document ? (default: Home)', 'Home', function ($answer) use ($documentName) {
                if (empty($answer)) {
                    throw new \RuntimeException(
                        'The name of the document cannot be empty.'
                    );
                }

                // if contains special characters
                if  (preg_match('/[!@#$%^&*()_+{}\[\]:;<>,.?~]/', $answer > 0)) {
                    throw new \RuntimeException('The document name cannot include specials characters.');
                }

                // trim
                $answer = trim($answer);

                // if many words separated by a space, capitalize the first letter of each word except the first one
                $fieldWords = explode(' ', $answer);
                foreach($fieldWords as $key => $word) {
                    if($key > 0) $fieldWords[$key] = ucfirst($word);
                }

                // get the camelCase field name
                $newFieldName = implode('', $fieldWords);

                return $newFieldName;
            });
        }

        // capitalize
        $documentName = ucfirst($documentName);

        // check if the document already exists
        if(file_exists('src/Document/' . $documentName . '.php')
            || file_exists('src/DTO/' . $documentName . '.php')
            || file_exists('src/DTO/' . $documentName . 'DTO' . '.php')
        ) $updateExistingDocument = true;

        // if update a dto, get the document name
        $documentName = str_replace('DTO', '', $documentName);

        // ask fields questions
        $questions = $this->askFieldsQuestions($updateExistingDocument, $documentName, $input, $output);

        // create or update document
        if($updateExistingDocument) {
            if(file_exists('src/Document/' . $documentName . '.php')) $this->updateDocument($documentName, $questions, $input, $output);
            if(file_exists('src/DTO/' . $documentName . 'GetDTO.php'))$this->updateDTO($documentName, $questions, $input, $output);
            if(file_exists('src/Controller/' . $documentName . 'Controller.php'))$this->updateController($documentName, $questions, $input, $output);
        }
        else {
            if($crud) {
                $this->generateDocument($questions, $documentName, $input, $output);
                $this->generateController($documentName, $questions, $input, $output);
            }
            $this->createDTO($questions, $documentName, $crud, $input, $output);
        }

        return Command::SUCCESS;

    }

    public function createDTO($questions, $documentName, $isCrud, $input, $output) {
        $io = new SymfonyStyle($input, $output);

        $io->text('Creating DTO...');

        $phpTypes = $this->symfonyTypes();

        $documentName = $documentName . 'GetDTO';

        $dtoContent = <<<PHP
        <?php
        
        namespace App\DTO;

        use Symfony\Component\Serializer\Annotation\Groups;
        use Symfony\Component\Validator\Constraints as Assert;
        use OpenApi\Attributes\Property;

        class {$documentName}
        {
        
            public function __construct(
        

        PHP;

        if($isCrud) {
            $dtoContent .= <<<PHP
            #[Groups(["read","update", "create"])]
            public readonly string|array|null \$id = null,\n

        PHP;
        }


        foreach ($questions as $question) {

            if($question['type'] === "collection") {
//                $dtoContent .= <<<PHP
//                    #[Property(type: "{$phpTypes[$question["elemType"]]}[]")]
//                    #[Groups(["read","create","update"])]
//                    public readonly array|null \${$question['name']} = null, \n
//
//            PHP;
            }
            else {
                $dtoContent .= <<<PHP
                #[Groups(["read","create","update"])]
                public readonly {$phpTypes[$question['type']]}|array|null \${$question['name']} = null,\n
                          
            PHP;
            }
        }

        $dtoContent .= <<<PHP
        \n
            ) {}
        }
        PHP;

        if(!file_exists('src/DTO')) mkdir('src/DTO');
        $documentPath = getcwd() . '/src/DTO/' . $documentName . '.php';
        file_put_contents($documentPath, $dtoContent);

        $io->success('DTO Class ' . $documentName . ' has been created successfully !');

    }

    public function updateDTO( $documentName, $questions, $input, $output ) {

        $documentName = $documentName . 'GetDTO';
        $phptypes = $this->symfonyTypes();

        $io = new SymfonyStyle($input, $output);
        try {
            // Path to the file to update
            if(! file_exists('src/DTO/' . $documentName . '.php')) {
                throw new \Exception('The DTO file does not exist.');
            }
            $filePath = getcwd() . '/src/DTO/' . $documentName . '.php';

            // Get file content
            $newContent = file_get_contents($filePath);

            // generate new properties
            $newProps = "";
            foreach ($questions as $question) {

                if($question['type'] === "collection") {
//                    $newProps .= <<<PHP
//                    #[Property(type: "{$phptypes[$question["elemType"]]}[]")]
//                    #[Groups(["read","create","update"])]
//                    public readonly array|null \${$question['name']} = null, \n
//
//            PHP;
                }
                else {

                $newProps .= <<<PHP
                    #[Groups(["read","create","update"])]
                    public readonly {$phptypes[$question['type']]}|array|null \${$question['name']} = null, \n
                                  
                PHP;
                }
            }

            // insert new properties just before after the last parenthesis
            $constructPos = strrpos($newContent, ')');
            if ($constructPos !== false) {
                // Trouver la position du début de la ligne contenant la position trouvée
                $lineStart = strrpos(substr($newContent, 0, $constructPos), "\n") + 1;

                // Insert new properties jst before the construct line
                $newContent = substr_replace($newContent, $newProps . "\n", $lineStart, 0);
            }

            // Update file
            file_put_contents($filePath, $newContent);

            $io->success($documentName . ' has been updated successfully.');
        }
        catch (\Throwable $e) {
            // Erreur lors de l'affectation de la propriété
            $io->text($e->getMessage());
        }

    }

    public function askFieldsQuestions($updateExistingDocument, $documentName, $input, $output)
    {
        $fieldsWithTypes = [];
        $io = new SymfonyStyle($input, $output);

        // first question to create fields
        $questionContent = $updateExistingDocument ? $documentName . ' already exists, do you want add a new field ?' : "Do you want add a new field ?";
        $setProps =  $io->confirm($questionContent, true);


        // questions
        while($setProps) {

            $fieldName = $io->ask('What is the name of your field ? (ex: name)', 'name', function ($answer) use ($documentName, $fieldsWithTypes, $updateExistingDocument) {

                // if empty
                if (empty($answer)) {
                    throw new \RuntimeException(
                        'The name of the field cannot be empty.'
                    );
                }

                // if contains special characters
                if  (preg_match('/[!@#$%^&*()_+{}\[\]:;<>,.?~]/', $answer) > 0) {
                    throw new \RuntimeException('The field name cannot include specials characters.');
                }

                // to lowercase
                $answer = trim($answer);

                // if many words separated by a space, capitalize the first letter of each word except the first one
                $fieldWords = explode(' ', $answer);
                foreach($fieldWords as $key => $word) {
                    if($key > 0) $fieldWords[$key] = ucfirst($word);
                }

                // get the camelCase field name
                $newFieldName = implode('', $fieldWords);

                //  check if this field name already exist in array
                $otherFieldsNames = array_column($fieldsWithTypes, 'name');

                // if the field already exists -> error
                if(in_array($newFieldName, $otherFieldsNames)) {
                    throw new \RuntimeException('The document ' . $documentName . ' already has a field named ' . $newFieldName . '.');
                }


                // check if this field already exist in the document when update
                if($updateExistingDocument) {

                    if(file_exists('src/Document/' . $documentName . '.php')) $filePath = getcwd() . '/src/Document/' . $documentName . '.php';
                    else if(file_exists('src/DTO/' . $documentName . 'DTO' . '.php')) $filePath = getcwd() . '/src/DTO/' . $documentName . 'DTO' . '.php';
                    else throw new \RuntimeException('The document ' . $documentName . ' does not exist.');

                    // Obtenez le contenu actuel du fichier
                    $newContent = file_get_contents($filePath);

                    // Utilise une expression régulière pour extraire les mots commençant par "$"
                    $pattern = '/\$\w+/';

                    // Effectue la recherche et stocke les correspondances dans $matches
                    preg_match_all($pattern, $newContent, $matches);

                    $variableNames = array_map(function($match) {
                        return ltrim($match, '$');
                    }, $matches[0]);

                    if(in_array($newFieldName, $variableNames)) {
                        throw new \RuntimeException('The document ' . $documentName . ' already has a field named ' . $newFieldName . '.');
                    }

                }
                return $newFieldName;
            });

            $fieldType = $io->choice('Select the type for your field', ['string', 'int', 'float', 'bool', 'collection', 'hash', 'date', 'timestamp', 'boolean'], 'string');

            if($fieldType === "collection") {
                $elemType = $io->choice('Select the type for your array elements', ['string', 'int', 'float', 'bool', 'collection', 'hash', 'date', 'timestamp', 'boolean'], 'string');
            }

            $fieldNullable = $io->confirm('Is your field nullable ?', true);

            $fieldsWithTypes[] = [
                'name' => $fieldName,
                'type' => $fieldType,
                'elemType' => $elemType ?? null,
                'nullable' => $fieldNullable
            ];

            $setProps = $io->confirm('Do you want add a new field ?', true);
        }

        return $fieldsWithTypes;
    }

    public function generateDocument ( $questions, $documentName, $input, $output ) : void
    {
        $io = new SymfonyStyle($input, $output);
        $questionsTypes = array_column($questions, "types");

        $content = <<<PHP
        <?php
        namespace App\Document;\n


        PHP;

        $content .= $this->generateUse($questionsTypes);

        $content .= <<<PHP
        #[Document]\n
        class {$documentName} {\n
        
        
        PHP;


        $content .= $this->generatePropertiesForCrudDocument($questions);

        $content .= <<<PHP
            public function __construct(){
            
            }\n

        PHP;


        $content .= $this->generateGettersAndSetters($questions, $documentName);

        $content .= <<<PHP
        
        }
        PHP;

        try {
            if(!file_exists('src/Document')) mkdir('src/Document');
            $documentPath = getcwd() . '/src/Document/' . $documentName . '.php';
            file_put_contents($documentPath, $content);

            $io->success('Class ' . $documentName . ' has been created successfully !');
        }
        catch (\Throwable $e) {
            // Erreur lors de l'affectation de la propriété
            $io->text($e->getMessage());
        }
    }

    public function updateDocument( $documentName, $questions, $input, $output ) : void
    {
        $io = new SymfonyStyle($input, $output);
        try {
            // Path to the file to update
            if(!file_exists('src/Document/'. $documentName . '.php')) {
                throw new \RuntimeException('The document ' . $documentName . ' does not exist.');
            }
            $filePath = getcwd() . '/src/Document/' . $documentName . '.php';

            // Get file content
            $newContent = file_get_contents($filePath);

            // generate new properties
            $newProps = $this->generatePropertiesForCrudDocument($questions, true);

            // insert new properties just before the contructor
            $constructPos = strpos($newContent, '__construct');
            if ($constructPos !== false) {
                // Trouver la position du début de la ligne contenant la position trouvée
                $lineStart = strrpos(substr($newContent, 0, $constructPos), "\n") + 1;

                // Insert new properties jst before the construct line
                $newContent = substr_replace($newContent, $newProps . "\n", $lineStart, 0);
            }

            // generate new getters and setters
            $gettersAndSetters = $this->generateGettersAndSetters($questions, $documentName, true);

            // get pos of last closing bracket in the file
            $lastClosingBracePos = strrpos($newContent, '}');

            // insert getters and setters just before the last closing bracket of the class
            if ($lastClosingBracePos !== false) {
                // get the start pos of the line that contains the last closing bracket
                $lineStart = strrpos(substr($newContent, 0, $lastClosingBracePos), "\n") + 1;

                // Insert new getters and setters just before the last closing bracket
                $newContent = substr_replace($newContent, $gettersAndSetters . "\n", $lineStart, 0);
            }

            // Update file
            file_put_contents($filePath, $newContent);

            $io->success($documentName . ' has been updated successfully.');
        }
        catch (\Throwable $e) {
            // Erreur lors de l'affectation de la propriété
            $io->text($e->getMessage());
        }
    }

    private function generateUse( $questionsTypes )
    {
        $useContent = <<<PHP
        use Symfony\Component\Validator\Constraints as Assert;
        use Symfony\Component\Serializer\Annotation\Groups;
        use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
        use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
        use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
        use OpenApi\Attributes\Property;


        PHP;


        foreach ($questionsTypes as $type) {
            if($type === "boolean") {
                $useContent .= <<<PHP
                use phpDocumentor\Reflection\Types\Boolean;
                PHP;
            }

            if($type === "timestamp") {
                $useContent .= <<<PHP
                use MongoDB\BSON\Timestamp;
                PHP;
            }
        }

        return $useContent;
    }

    private function generatePropertiesForCrudDocument($questions, $update=false) {

        $content = "";

        if(!$update) {
            $content = <<<PHP
            #[Id]
            #[Groups(["read","update", "create"])]
            private string \$id;\n

        PHP;
        }


        foreach ($questions as $question) {

            $isNullable = $question['nullable'];

            $type = $question['type'];
            $phpType = $isNullable ? "?".$this->symfonyTypes()[$type] : $this->symfonyTypes()[$type];

            $name = $question['name'];

            $nullable = $isNullable ? ', nullable: true' : '';

            if(!$nullable) {
                $content .= <<<PHP
                    #[Assert\NotBlank(message: "{$name} is required", groups: ["create"])]\n
                PHP;
            }

            if($question['type'] === "collection") {
                $content .= <<<PHP
                    #[Property(type: "{$this->symfonyTypes()[$question["elemType"]]}[]")]\n
                PHP;
            }

            $content .= <<<PHP
                #[Groups(["read","update", "create"])]
                #[Field(type: "{$type}" {$nullable})]
                private {$phpType} \${$name}; \n

            PHP;
        }

        return $content;
    }

    private function generateGettersAndSetters ($questions, $documentName, $update=false) {

        $content = "";

        if(!$update) {
            $content .= <<<PHP
                /**
                 * @return string
                 */
                public function getId(): string
                {
                    return \$this->id;
                }\n

            PHP;

        }

        foreach ($questions as $question) {

            $nullable = $question["nullable"];
            $name = $question["name"];
            $capitalizedName = ucfirst($name);
            $type = $question["type"];
            $phpType =  $this->symfonyTypes()[$type];
            $annotationType = $nullable ? $phpType.'|null' : $phpType;
            $getterFunctionReturn = $nullable ? "?".$phpType : $phpType;

            $content .= <<<PHP
        /**
         * @return {$annotationType}
         */
        public function get{$capitalizedName}(): {$getterFunctionReturn}
        {
            return \$this->{$name};
        }

        /**
         * @param {$annotationType} \${$name}
         * @return {$documentName}
         */
        public function set{$capitalizedName}({$getterFunctionReturn} \${$name}): self
        {
            \$this->{$name} = \${$name};
            return \$this;
        }\n

    PHP;

        }

        return $content;
    }

    private function generateController ( $documentname, $questions, $input, $output ) {

        $io = new SymfonyStyle($input, $output);
        $controllerName = $documentname . "Controller";
        
        $dtoName = $documentname . "GetDTO";
        $lowerDtoName = strtolower($dtoName);
        $lowerDocumentName = strtolower($documentname);

        $routeName = '/api/'. strtolower($documentname);
        $routeNameWithIdParam = $routeName . '/{id}';
        
        $content = <<<PHP
        <?php

        namespace App\Controller;
        
        
        use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
        use Symfony\Component\HttpFoundation\JsonResponse;
        use Symfony\Component\HttpFoundation\Response;
        use Symfony\Component\Routing\Annotation\Route;
        use Symfony\Component\HttpKernel\Attribute\MapQueryString;
        use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
        use Symfony\Component\Serializer\SerializerInterface;
        use Symfony\Component\Validator\Validator\ValidatorInterface;
        use Doctrine\ODM\MongoDB\DocumentManager;
        use OpenApi\Attributes as OA;
        use Ama\SymfonyMongoMakerBundle\DTO\QueryDTO;
        use Ama\SymfonyMongoMakerBundle\Service\QueryBuilderFromUrlService;
        use App\Document\\{$documentname};
        use App\DTO\\{$dtoName};
        use Nelmio\ApiDocBundle\Annotation\Model;


        class {$documentname}Controller extends AbstractController
        {
        
            public function __construct(
                private DocumentManager \$dm,
                private ValidatorInterface \$validator,
                private SerializerInterface \$serializer,
                private QueryBuilderFromUrlService \$queryBuilderFromUrlService
            ){}
        
        
            #[Route("{$routeName}", methods: ['GET'])]
            #[OA\Response(
                response: 200,
                description: 'Returns an array of {$lowerDocumentName}',
                content: new OA\JsonContent(
                    properties: [
                        new OA\Property(
                            property: 'data',
                            type: 'array',
                            items: new OA\Items(ref: new Model(type: {$documentname}::class, groups: ['read'])),
                        ),
                        new OA\Property(
                            property: 'error',
                            type: 'string|bool',
                        ),
                    ],
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: {$documentname}::class, groups: ['read']))
                )
            )]
            #[OA\Parameter(
                name: 'id',
                description: 'id- field of {$dtoName}',
                in: 'query',
                schema: new OA\Schema(type: 'string|string[]|null')
            )]\n
        PHP;

        foreach ($questions as $question) {
            $phpType = $this->symfonyTypes()[$question['type']];
            if($question['type'] !== "collection") {
                $content .= <<<PHP
                #[OA\Parameter(
                    name: '{$question['name']}',
                    description: '{$question['name']} - field of {$dtoName}',
                    in: 'query',
                    schema: new OA\Schema(type: '{$phpType}|{$phpType}[]|null')
                )]\n
            PHP;
            }
        }

            $content .= <<<PHP
            #[OA\Parameter(
                name: 'sort',
                description: 'array to sort query by fields name with asc or desc values. Example sort[date]=asc&sort[name]=desc will sort the results by date and by name',
                in: 'query',
                schema: new OA\Schema(type: 'string')
            )]
            #[OA\Parameter(
                name: 'nested',
                description: 'array to search into nested objects fields. Example nested[user.website.id]=4 will search for a user with a website with id 4',
                in: 'query',
                schema: new OA\Schema(type: 'string')
            )]
            public function findBy ( #[MapQueryString] ?{$dtoName} \${$lowerDtoName}, #[MapQueryString] ?QueryDTO \$queryDTO ) :JsonResponse
            {
                // if no params, return the result of findAll
                \$result = \$this->queryBuilderFromUrlService->getResult(\$this->dm, {$documentname}::class, \${$lowerDtoName}, \$queryDTO);
        
                // normalize results
                \$normalizedResult = \$this->serializer->normalize(\$result, null, ["groups" => ['read']]);
        
                // return result
                return new JsonResponse([
                    "data" => \$normalizedResult,
                    "error" => false
                ], Response::HTTP_OK);
            }
        
        
        
        
        
            #[Route("{$routeNameWithIdParam}", methods: ['PUT'])]
            #[OA\Response(
                response: 200,
                description: 'Returns the {$lowerDocumentName} updated',
                content: new OA\JsonContent(
                    properties: [
                        new OA\Property(
                            property: 'data',
                            ref: new Model(type: {$documentname}::class, groups: ['read']),
                            type: 'object'
                        ),
                        new OA\Property(
                            property: 'error',
                            type: 'string | false'
                        )
                    ],
                    type: 'object',
                )
            )]
            #[OA\Parameter(
                name: 'id',
                description: '{$lowerDocumentName} id',
                in: 'path',
                schema: new OA\Schema(type: 'string')
            )]
            public function update ( #[MapRequestPayload] {$dtoName} \${$lowerDtoName}, \$id ) :JsonResponse
            {
                \$documentToUpdate = \$this->dm->find({$documentname}::class, \$id);
        
                if(empty(\$documentToUpdate)) {
                    return new JsonResponse([
                        "data" => false,
                        "error" => "no test matching with id " . \$id . " in database"
                    ], Response::HTTP_OK);
                }
        
        
                \${$lowerDtoName} = array_filter(json_decode(json_encode(\${$lowerDtoName}), true), fn(\$value) => \$value !== null);
                \$this->dm->getHydratorFactory()->hydrate(\$documentToUpdate, \${$lowerDtoName});
                \$this->dm->persist(\$documentToUpdate);
                \$this->dm->flush();
        
        
        
                // return result
        
                return new JsonResponse([
                    "data" => \$this->serializer->normalize(\$documentToUpdate, null, ['groups' => ['read']] ),
                    "error" => false
                ], Response::HTTP_OK);
        
            }
        
        
            #[Route("{$routeName}", methods: ['POST'])]
            #[OA\Response(
                response: 200,
                description: "Returns an object with 'data' key (object | false) that provides the new document created and 'error' key (string | false)",
                content: new OA\JsonContent(
                    properties: [
                        new OA\Property(
                            property: 'data',
                            ref: new Model(type: {$documentname}::class, groups: ['read']),
                            type: 'object'
                        ),
                        new OA\Property(
                            property: 'error',
                            type: 'string | false'
                        )
                    ],
                    type: 'object',
                )
            )]
            public function create( #[MapRequestPayload] {$lowerDocumentName} \${$lowerDocumentName} ): JsonResponse
            {
                \$errors = \$this->validator->validate(\${$lowerDocumentName}, null, ['create']);
        
                if(count(\$errors) > 0) {
                    \$errorsString = implode(', ', array_map(fn(\$error) => \$error->getMessage(), iterator_to_array(\$errors)));
                    return new JsonResponse([
                        "data" => false,
                        "error" => \$errorsString
                    ], Response::HTTP_BAD_REQUEST);
                }
        
                \$this->dm->persist(\${$lowerDocumentName});
                \$this->dm->flush();
        
                \${$lowerDocumentName} = \$this->serializer->normalize(\${$lowerDocumentName}, null, ['groups' => ['read']] );
        
                return new JsonResponse([
                    "data" => \${$lowerDocumentName},
                    "error" => false
                ], Response::HTTP_OK);
        
            }
        
            #[Route("{$routeNameWithIdParam}", methods: ['DELETE'])]
            #[OA\Response(
                response: 200,
                description: "Returns an object with 'data' key (string | false) that confirm the deletion and 'error' key (string | false)",
                content: new OA\JsonContent(
                    properties: [
                        new OA\Property(
                            property: 'data',
                            type: 'string'
                        ),
                        new OA\Property(
                            property: 'error',
                            type: 'string | false'
                        )
                    ],
                    type: 'object',
                )
            )]
            #[OA\Parameter(
                name: 'id',
                description: '{$lowerDocumentName} id',
                in: 'path',
                schema: new OA\Schema(type: 'string')
            )]
            public function delete ( \$id ) :JsonResponse
            {
                \$document = \$this->dm->find({$documentname}::class, \$id);
        
                if(empty(\$document)) {
                    return new JsonResponse([
                        "data" => false,
                        "error" => "no test matching with this id in database"
                    ], Response::HTTP_BAD_REQUEST);
                }
                else {
                    \$this->dm->remove(\$document);
                    \$this->dm->flush();
                }
                return new JsonResponse([
                    "data" => "document removed successfully",
                    "error" => false
                ], Response::HTTP_OK);
            }
        }
        PHP;

        // Update file
        $filePath = getcwd() . "/src/Controller/" . $controllerName . ".php";
        file_put_contents($filePath, $content);

        $io->success($controllerName . ' has been created successfully.');

    }

    private function updateController ( $documentName, $questions, $input, $output ) {
        $io = new SymfonyStyle($input, $output);
        $dtoName = $documentName . 'DTO';
        try {
            // Path to the file to update
            if(!file_exists('src/Controller/'. $documentName . 'Controller.php')) {
                throw new \RuntimeException('The controller ' . $documentName . 'Controller does not exist.');
            }
            $filePath = getcwd() . '/src/Controller/' . $documentName . 'Controller.php';

            // Get file content
            $newContent = file_get_contents($filePath);

            // generate new properties
            $newProps = "";

            foreach ($questions as $question) {
                $phpType = $this->symfonyTypes()[$question['type']];
                if($question['type'] !== 'collection') {
                    $newProps .= <<<PHP
                    #[OA\Parameter(
                        name: '{$question['name']}',
                        description: '{$question['name']} - field of {$dtoName}',
                        in: 'query',
                        schema: new OA\Schema(type: '{$phpType}|{$phpType}[]|null')
                    )]
                PHP;
                }

            }

            // insert new properties just before the contructor
            $findByPos = strpos($newContent, 'public function findBy');
            if ($findByPos !== false) {
                // Trouver la position du début de la ligne contenant la position trouvée
                $lineStart = strrpos(substr($newContent, 0, $findByPos), "\n") + 1;

                // Insert new properties jst before the construct line
                $newContent = substr_replace($newContent, $newProps . "\n", $lineStart, 0);
            }

            // Update file
            file_put_contents($filePath, $newContent);

            $io->success($documentName . 'Controller has been updated successfully.');
        }
        catch (\Throwable $e) {
            // Erreur lors de l'affectation de la propriété
            $io->text($e->getMessage());
        }
    }

    public function symfonyTypes () {
        return [
            "string" => "string",
            "int"    => "int",
            "float"  => "float",
            "collection" => "array",
            "hash"   => "array",
            "bool"   => "bool",
            "boolean" => "Boolean",
            "date" => "\DateTime",
            "timestamp" => "timestamp"
        ];
    }

}
