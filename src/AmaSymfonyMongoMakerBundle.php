<?php

// Copyright (c) 2023 Alexandra Masvidal
// MIT License
// For the complete copyright and license information, please refer to the LICENSE file.

namespace Ama\SymfonyMongoMakerBundle;

use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

use Ama\SymfonyMongoMakerBundle\Command\MongoControllerMakerCommand;
use Ama\SymfonyMongoMakerBundle\Command\MongoDocumentMakerCommand;


/**
 * SymfonyMongoMakerBundle
 */
class AmaSymfonyMongoMakerBundle extends AbstractBundle
{

    public function getPath(): string
    {
        return __DIR__;
    }

    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../../config')
        );
        $loader->load('services.yaml');

        $this->addAnnotatedClassesToCompile([
            'App\\Command\\MongoControllerMakerCommand',
            'App\\Command\\MongoDocumentMakerCommand',
            'App\\DTO\\QueryDTO',
            'App\\Service\\QueryBuilderFromUrlService',
        ]);
    }

    public function loadExtension(array $config, ContainerConfigurator $containerConfigurator, ContainerBuilder $containerBuilder): void
    {
        // load an XML, PHP or Yaml file
        $containerConfigurator->import('../config/services.yaml');

    }

}
