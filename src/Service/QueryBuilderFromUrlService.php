<?php

namespace Ama\SymfonyMongoMakerBundle\Service;

use App\Enum\QueryParameters;
use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\BSON\Regex;
use ReflectionClass;
use Symfony\Component\HttpKernel\Exception\HttpException;


class QueryBuilderFromUrlService
{

    public function getResult(DocumentManager $dm, $className, $documentDTO, $queryDTO)
    {
        // associative array with "paramName" => "paramType"
        $classPropertiesAndTypes = $this->getPropertiesWithTypesFromClass($className);

        // if no params -> findall
        if(empty($documentDTO) && empty($queryDTO)) $result = $dm->getRepository($className)->findAll();

        // create custom query
        else {
            // init query
            $query = $dm->createQueryBuilder($className);

            // get array from std class
            $documentDTO = array_filter(json_decode(json_encode($documentDTO), true), fn($value) => $value !== null);
            $documentDTOKeys = array_keys($documentDTO);

            // add document params query
            if(!empty($documentDTO)) $query = $this->getQueryFromDocumentDTO($documentDTO, $query);

            // handle strategy params
            if(!empty($queryDTO)) $query = $this->getQueryFromQueryDTO( $queryDTO, $documentDTOKeys, $query );

            $result = $query->getQuery()->toArray();
        }

        return $result;
    }


    /**
     * @param $paramName
     * @param $query
     * @param $params
     * @return mixed
     */
    private function getStringQuery($paramName, $query, $params) {

        // get string comparators
        $keys = array_keys($params);
        $pattern = '';

        if(in_array(QueryParameters::like->value, $keys)) {
            $pattern = $params[QueryParameters::like->value];
        }
        else {
            if(in_array(QueryParameters::start->value, $keys)) {
                $pattern .= '^'.$params[QueryParameters::start->value].'.*';
            }
            if(in_array(QueryParameters::istart->value, $keys)) {
                $pattern .= '^'.$params[QueryParameters::istart->value].'.*';
            }
            if(in_array(QueryParameters::end->value, $keys)) {
                $pattern .= $params[QueryParameters::end->value].'$';
            }
            if(in_array(QueryParameters::iend->value, $keys)) {
                $pattern .= $params[QueryParameters::iend->value].'$';
            }
        }
        $option = QueryParameters::containsCaseInsensistiveComparator($keys) ? 'i' : '';

        $regex = new Regex($pattern, $option);

        return $query->field($paramName)->equals($regex);
    }


    public function getPropertiesWithTypesFromClass ($classname)
    {
        $reflectionClass = new ReflectionClass($classname);
        $properties = $reflectionClass->getProperties();
        $types = [];

        foreach ($properties as $property) {
            $types[$property->getName()] = $property->getType()->getName();
        }

        return $types;
    }

    public function getQueryFromDocumentDTO ( $documentDTO, $query ) {



        // handle every param key/value
        foreach ($documentDTO as $paramName => $paramValues) {

            // if paramsValues is not an array
            if(!is_array($paramValues))  $query = $query->field($paramName)->equals($paramValues);

            else {
                //get comparators
                $comparators = array_keys($paramValues);
                QueryParameters::validateComparators($comparators);

                // get only string comparators
                $comparators = array_filter($comparators, fn($c) => gettype($c) === "string");

                // find in list
                if(empty($comparators)) {
                    $values = array_values($paramValues);
                    $query = $query->field($paramName)->in($values);
                }

                else {
                    // update query for number/date comparators
                    $isNumberComparator = QueryParameters::getComparatorType($comparators[0]) === "number";
                    $isStringComparators = QueryParameters::getComparatorType($comparators[0]) === "string";
                    $isDateComparators = QueryParameters::getComparatorType($comparators[0]) === "date";

                    if($isNumberComparator) {
                        foreach($paramValues as $comparatorName => $comparatorValue) {
                            $comparatorValue = floatval($comparatorValue);
                            $query = $query->field($paramName)->$comparatorName($comparatorValue);
                        }
                    }

                    // update query for string comparators
                    if($isStringComparators) $query = $this->getStringQuery($paramName, $query, $paramValues);
                }
            }
        }

        return $query;
    }

    public function getQueryFromQueryDTO( $queryDTO, $documentDTOKeys, $query )
    {
        // search in nested objects
        if(!empty($queryDTO->nested)) {
            foreach ($queryDTO->nested as $key => $value) {
                $query = $query->field($key)->equals($value);
            }
        }

        if(!empty($queryDTO->sort)){
            $sort = json_decode(json_encode($queryDTO->sort), true);
            $sort = array_map(fn($value) => strtolower($value), $sort);
            $sort = array_filter($sort, function($value, $key) use ($documentDTOKeys) {
                return in_array($key, $documentDTOKeys) && ($value === "asc" || $value === "desc");
            },ARRAY_FILTER_USE_BOTH);

            foreach ($sort as $key => $value) {
                $query = $query->sort($key, $value);
            }
        }

        return $query;
    }


}