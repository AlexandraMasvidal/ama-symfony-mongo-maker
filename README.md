<!-- Copyright (c) 2023 Alexandra Masvidal -->
<!-- MIT License -->
<!-- For the complete copyright and license information, please refer to the LICENSE file. -->

Mongo bundle - development in progress...

This bundle is my first one, for private projects for the moment. 
[Link to my repo here](https://gitlab.com/AlexandraMasvidal/ama-symfony-mongo-maker)


It implements two symfony commands :
    
    1 - mongo:make:document --nocrud name

    * name is optionnal, it will be ask as first question if it's not provided
    * --nocrud prevent creation of document and a controller and creates only the DTO class

    2 - mongo:make:controller (with no option, allow to create controllers not linked to a document - usefull for gateways api)
    
The maker works as the symfony original maker for orm entities:

It will ask the name of the document (or controller) and then, for each property :the name, the type and if it is nullable
Then, it will create a document, a functionnal crudcontroller and a dto class
If --nocrud option is not provided, the document and the controller are implemented with nelmio basic documentation to create a swagger